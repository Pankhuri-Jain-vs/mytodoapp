/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect} from 'react';
import {SafeAreaView, ScrollView, StatusBar} from 'react-native';
import {Provider} from 'react-redux';
import * as Sentry from '@sentry/react-native';

import UserListingScreen from './src/screens/user-list/UserListingScreen';
import {store} from './src/store/store';

Sentry.init({
  dsn: 'https://2e5856e890da4d4e9ab4bd6bc3699786@o1015623.ingest.sentry.io/5981273',
  integrations: [
    new Sentry.ReactNativeTracing({
      tracingOrigins: ['localhost', /^\//, /^https:\/\//],
      // tracingOrigins: ['localhost', 'my-site-url.com', /^\//],
      // ... other options
    }),
  ],
  tracesSampleRate: 1.0,
  release: 'myTodoApp@1.2.0',
  // release health
  enableAutoSessionTracking: true,
  // Sessions close after app is 10 seconds in the background.
  sessionTrackingIntervalMillis: 10000,
});
const App = () => {
  return (
    <Provider store={store}>
      <SafeAreaView>
        <StatusBar />
        <ScrollView>
          <UserListingScreen />
        </ScrollView>
      </SafeAreaView>
    </Provider>
  );
};
// export default Sentry.wrap(App);
export default App;

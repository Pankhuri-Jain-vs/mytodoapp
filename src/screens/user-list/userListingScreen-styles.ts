import {StyleSheet, TextStyle, ViewStyle} from 'react-native';

interface IStyles {
  container: ViewStyle;
  subContainer: ViewStyle;
  headerContainer: ViewStyle;
  headerTitle: TextStyle;
}

const styles = StyleSheet.create<IStyles>({
  container: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  subContainer: {
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 4,
    marginBottom: 32,
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  headerContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  headerTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
});

export default styles;

import React, {useEffect, useState} from 'react';
import {Text, ScrollView, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import * as Sentry from '@sentry/react-native';

import Modal from '../../components/modal/Modal';
import NoteContainer from '../../components/note-container/NoteContainer';
import UserContainer from '../../components/user-container/UserContainer';
import {IRootReducer} from '../../store/store';
import {IUser} from '../../common/types';
import {addNewNote, fetchNoteList} from '../../store/notesStore/actions';
import {fetchUsers} from '../../store/userStore/actions';

import styles from './userListingScreen-styles';

const UserListingScreen = () => {
  const [inputValue, setInputValue] = useState<string>('');
  const [showModal, setShowModal] = useState<boolean>(false);
  const [showTodo, setShowTodo] = useState<number>(-1);
  const [currentUserId, setCurrentUserId] = useState<number>(null);

  const {users, isLoading, errMsg} = useSelector(
    (state: IRootReducer) => state.userStore,
  );

  const {
    notes,
    isLoading: isNoteLoading,
    errMsg: fetchNoteErrMsg,
  } = useSelector((state: IRootReducer) => state.notesStore);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchUsers());
  }, []);

  const handleInputChange = (value: string): void => {
    setInputValue(value);
  };

  const handleSavePress = (value: string): void => {
    const newNote = {
      due_on: '2021-10-06T00:00:00.000+05:30', //new Date()
      id: Math.floor(Math.random() * 1000) + 1,
      status: 'pending',
      title: value,
      user_id: currentUserId,
    };
    setShowModal(false);
    dispatch(addNewNote(newNote));
  };

  const handleCancelPress = (): void => {
    setShowModal(false);
  };

  const handleShowTodo = (userId: number): void => {
    setShowTodo(userId);
    setCurrentUserId(userId);
    dispatch(fetchNoteList(userId));
  };

  const addNewNoteClicked = (): void => {
    setInputValue('');
    setShowModal(true);
  };

  const todoContainer = (): React.ReactNode => {
    return !isNoteLoading ? (
      <NoteContainer notes={notes} addNewNoteClicked={addNewNoteClicked} />
    ) : (
      <View>
        <Text>Loading...</Text>
      </View>
    );
  };

  return !isLoading ? (
    <>
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={styles.container}>
        <View style={styles.headerContainer}>
          <Text style={styles.headerTitle}>Users List</Text>
        </View>
        {!errMsg ? (
          users.map((user: IUser) => (
            <View style={styles.subContainer} key={user.id}>
              <UserContainer user={user} handleShowTodo={handleShowTodo} />
              {showTodo === user.id && todoContainer()}
            </View>
          ))
        ) : (
          <Text>there is er</Text>
        )}
      </ScrollView>
      {showModal ? (
        <Modal
          inputValue={inputValue}
          handleInputChange={handleInputChange}
          handleSavePress={handleSavePress}
          handleCancelPress={handleCancelPress}
        />
      ) : null}
    </>
  ) : (
    <View>
      <Text>Loading...</Text>
    </View>
  );
};

export default UserListingScreen;
// export default Sentry.withProfiler(UserListingScreen);

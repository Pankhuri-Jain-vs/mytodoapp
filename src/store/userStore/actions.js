import axios from 'axios';
import {Dispatch} from 'redux';
// import Joi from 'joi';
const Joi = require('joi');

import * as types from './types';
import {IUser} from '../../common/types';

export const fetchUsersSuccess = users => {
  return {type: types.FETCH_USERS_SUCCESS, payload: users};
};

export const fetchUsersFailure = message => {
  return {type: types.FETCH_USERS_FAILURE, payload: message};
};

export const fetchUsersInProgress = value => {
  return {type: types.FETCH_USERS_IN_PROGRESS, payload: value};
};

const schema = Joi.array().items(
  Joi.object().keys({
    id: Joi.number().required(),
    name: Joi.string().required(),
    email: Joi.string()
      .email({tlds: {allow: false}})
      .required(),
    status: Joi.number(),
    gender: Joi.string(),
  }),
);

export const fetchUsers = () => {
  return dispatch => {
    dispatch(fetchUsersInProgress(true));
    axios
      .get('https://gorest.co.in/public/v1/users')
      .then(resp => {
        const users = resp.data.data;
        const value = schema.validate(users);
        console.log('value', value);
        // if (value.error) {
        //   throw value.error;
        // }
        console.log('Joi.isError();', Joi.isError(value.error));
        dispatch(fetchUsersSuccess(users));
      })
      .catch(err => {
        dispatch(fetchUsersFailure(err.message));
      })
      .finally(() => {
        dispatch(fetchUsersInProgress(false));
      });
  };
};

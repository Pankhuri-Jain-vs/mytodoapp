import {IUser} from '../../common/types';

export const FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS';
export const FETCH_USERS_FAILURE = 'FETCH_USERS_FAILURE';
export const FETCH_USERS_IN_PROGRESS = 'FETCH_USERS_IN_PROGRESS';

export interface IReturnType {
  type: string;
  payload: boolean | string | Array<IUser>;
}

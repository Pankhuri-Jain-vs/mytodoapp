import {IUser} from '../../common/types';
import * as types from './types';

export interface IUserInitialState {
  isLoading: boolean;
  users: Array<IUser>;
  errMsg: string;
}

const initialState = {
  isLoading: false,
  users: [],
  errMsg: '',
};

// todo discuss this here :IUserInitialState
export const userReducer = (
  state: IUserInitialState = initialState,
  action: types.IReturnType,
) => {
  switch (action.type) {
    case types.FETCH_USERS_SUCCESS:
      return {...state, errMsg: '', users: action.payload};
    case types.FETCH_USERS_IN_PROGRESS:
      return {...state, isLoading: action.payload};
    case types.FETCH_USERS_FAILURE:
      return {...state, errMsg: action.payload};
    default:
      return state;
  }
};

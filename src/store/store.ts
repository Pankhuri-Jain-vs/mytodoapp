import {applyMiddleware, createStore, combineReducers} from 'redux';
import thunk from 'redux-thunk';

import {INotesInitialState, notesReducer} from './notesStore/reducer';
import {IUserInitialState, userReducer} from './userStore/reducer';

export interface IRootReducer {
  userStore: IUserInitialState;
  notesStore: INotesInitialState;
}

const rootReducer = combineReducers({
  userStore: userReducer,
  notesStore: notesReducer,
});
export const store = createStore(rootReducer, applyMiddleware(thunk));

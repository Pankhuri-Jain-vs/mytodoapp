import {INote} from '../../common/types';
import * as types from './types';

export interface INotesInitialState {
  errMsg: string;
  notes: Array<INote>;
  isLoading: boolean;
}

const initialState = {
  errMsg: '',
  notes: [],
  isLoading: false,
};

export const notesReducer = (
  state: INotesInitialState = initialState,
  action: types.IReturnType,
) => {
  switch (action.type) {
    case types.ADD_NOTE:
      return {...state, notes: [...state.notes, action.payload]};
    case types.FETCH_NOTE_SUCCESS:
      return {...state, notes: action.payload};
    case types.FETCH_NOTE_FAILURE:
      return {...state, errMsg: action.payload};
    case types.FETCH_NOTE_IN_PROGRESS:
      return {...state, isLoading: action.payload};
    default:
      return state;
  }
};

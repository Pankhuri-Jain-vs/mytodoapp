import {INote} from '../../common/types';

export const ADD_NOTE = 'ADD_NOTE';
export const FETCH_NOTE_SUCCESS = 'FETCH_NOTE_SUCCESS';
export const FETCH_NOTE_FAILURE = 'FETCH_NOTE_FAILURE';
export const FETCH_NOTE_IN_PROGRESS = 'FETCH_NOTE_IN_PROGRESS';

export interface IReturnType {
  type: string;
  payload: boolean | string | Array<INote> | INote;
}

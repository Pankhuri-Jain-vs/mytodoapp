import axios from 'axios';
import {Dispatch} from 'redux';
import Joi from 'joi';

import {INote} from '../../common/types';
import * as types from './types';

export const fetchNoteSuccess = (notes: Array<INote>): types.IReturnType => {
  return {type: types.FETCH_NOTE_SUCCESS, payload: notes};
};

export const fetchNoteFailure = (message: string): types.IReturnType => {
  return {type: types.FETCH_NOTE_SUCCESS, payload: message};
};

export const fetchNoteInProgress = (loading: boolean): types.IReturnType => {
  return {type: types.FETCH_NOTE_IN_PROGRESS, payload: loading};
};

export const addNewNoteSuccess = (note: INote): types.IReturnType => {
  return {type: types.ADD_NOTE, payload: note};
};

const schema = Joi.array().items(
  Joi.object({
    id: Joi.number(),
    user_id: Joi.number(),
    title: Joi.string().required(),
    status: Joi.string(),
    due_on: Joi.date(),
  }),
);

// todo dispatch type
export const fetchNoteList = (userId: number) => {
  return (dispatch: Dispatch<types.IReturnType>) => {
    dispatch(fetchNoteInProgress(true));
    axios
      .get(`https://gorest.co.in/public/v1/users/${userId}/todos`)
      .then(resp => {
        const notes = resp.data.data;
        const value = schema.validate(notes);
        if (value.error) throw value.error;
        dispatch(fetchNoteSuccess(notes));
      })
      .catch(err => {
        dispatch(fetchNoteFailure(err));
      })
      .finally(() => {
        dispatch(fetchNoteInProgress(false));
      });
  };
};

const token =
  'ea799bc8dbca950235966f5fde5d35adb2531e0addcdb83743e0785b42b341c3';
const config = {
  headers: {Authorization: `Bearer ${token}`},
};

// todo dispatch type
export const addNewNote = (note: INote) => {
  return (dispatch: Dispatch<types.IReturnType>) => {
    dispatch(fetchNoteInProgress(true));
    axios
      .post(
        `https://gorest.co.in/public/v1/users/${note.user_id}/todos`,
        note,
        config,
      )
      .then(resp => {
        console.log('add resp', resp);
        dispatch(addNewNoteSuccess(resp.data.data));
      })
      .catch(err => {
        console.log('err', err);
      })
      .finally(() => {
        dispatch(fetchNoteInProgress(false));
      });
  };
};

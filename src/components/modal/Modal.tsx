import React, {useEffect} from 'react';
import {Button, View, TextInput} from 'react-native';
import * as Sentry from '@sentry/react-native';

import styles from './modal-styles';
import {createTransaction} from '../../common/utils';

interface IModal {
  inputValue: string;
  handleInputChange: (text: string) => void;
  handleSavePress: (text: string) => void;
  handleCancelPress: () => void;
}

const Modal: React.FC<IModal> = (props: IModal) => {
  const {
    inputValue = '',
    handleInputChange,
    handleSavePress,
    handleCancelPress,
  } = props;

  const handleSaveAndCreateTransaction = (inputValue: string) => {
    createTransaction('test-transaction', handleSavePress, inputValue);
  };

  return (
    <View style={styles.subContainer}>
      <TextInput
        onChangeText={handleInputChange}
        value={inputValue}
        placeholder="enter note here"
      />
      <Button
        title="save"
        onPress={() => handleSaveAndCreateTransaction(inputValue)}
      />
      <Button title="cancel" onPress={handleCancelPress} />
    </View>
  );
};

// export default Sentry.withProfiler(Modal);
export default Modal;

import {StyleSheet, ViewStyle} from 'react-native';

interface IStyles {
  container: ViewStyle;
  subContainer: ViewStyle;
}

const styles = StyleSheet.create<IStyles>({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: 'grey',
    opacity: 0.4,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  subContainer: {
    backgroundColor: 'white',
    opacity: 1,
    width: '50%',
    height: '30%',
  },
});

export default styles;

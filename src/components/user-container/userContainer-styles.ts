import {StyleSheet, ImageStyle, TextStyle} from 'react-native';

interface IStyles {
  title: TextStyle;
  icon: ImageStyle;
}

const styles = StyleSheet.create<IStyles>({
  title: {
    fontSize: 16,
  },
  icon: {
    width: 20,
    height: 20,
    marginRight: 5,
  },
});

export default styles;

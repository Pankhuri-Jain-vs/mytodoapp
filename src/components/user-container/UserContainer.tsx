import React from 'react';
import {Image, View, Text, TouchableOpacity} from 'react-native';
import * as Sentry from '@sentry/react-native';
import Joi from 'joi';

import {IUser} from '../../common/types';

import styles from './userContainer-styles';
import {createTransaction} from '../../common/utils';

interface IUserContainer {
  user: string;
  handleShowTodo: (text: number) => void;
}

const UserContainer: React.FC<IUserContainer> = (props: IUserContainer) => {
  const {user, handleShowTodo} = props;

  const schema = Joi.object({
    user: Joi.object({
      id: Joi.number().required(),
      name: Joi.string().required(),
      email: Joi.string()
        .email({tlds: {allow: false}})
        .required(),
      status: Joi.string(),
      gender: Joi.string(),
    }),
    handleShowTodo: Joi.function().arity(1),
  });
  // const validateValue = schema.validate(props);
  // console.log('validateValue', validateValue);

  const {name, email, id} = user;

  return (
    <View>
      <Text style={styles.title}>{name}</Text>
      <Text style={styles.title}>{email}</Text>
      <TouchableOpacity onPress={() => handleShowTodo(id)}>
        <Image
          source={require('../../assets/icons/edit.png')}
          style={styles.icon}
        />
      </TouchableOpacity>
    </View>
  );
};
export default UserContainer;

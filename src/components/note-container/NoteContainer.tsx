import React from 'react';
import {Text, Button, View} from 'react-native';

import {INote} from '../../common/types';

import styles from './noteContainer-styles';

interface NoteContainer {
  notes: Array<INote>;
  addNewNoteClicked: () => void;
}

const NoteContainer = props => {
  const {notes, addNewNoteClicked} = props;

  return (
    <View>
      {notes && notes.length ? (
        notes.map((note: INote) => {
          return (
            <Text style={styles.title} key={note.id}>
              -{note.title}
            </Text>
          );
        })
      ) : (
        <Text style={styles.title}>no todos yet, wohooo! enjoyyy</Text>
      )}
      <Button title="add new note" onPress={addNewNoteClicked} />
    </View>
  );
};

export default NoteContainer;

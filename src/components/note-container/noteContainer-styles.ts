import {StyleSheet, TextStyle} from 'react-native';

interface IStyles {
  title: TextStyle;
}

const styles = StyleSheet.create<IStyles>({
  title: {
    fontSize: 16,
    marginTop: 3,
    marginBottom: 5,
    color: 'blue',
  },
});

export default styles;

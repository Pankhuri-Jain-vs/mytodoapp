export interface INote {
  id: number;
  user_id: number;
  title: string;
  status: string;
  due_on: string;
}

export interface IUser {
  id: number;
  name: string;
  email: string;
  status: string;
  gender: string;
}

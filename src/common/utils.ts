import * as Sentry from '@sentry/react-native';

export const createTransaction = (
  key: string,
  func: (...args: any) => void,
  ...args
) => {
  console.log('trans start');
  const transaction = Sentry.startTransaction({
    name: key,
  });
  const span = transaction.startChild({op: 'function-tracking'}); // This function returns a Span
  func(...args);
  span.finish(); // Remember that only finished spans will be sent with the transaction
  transaction.finish(); // Finishing the transaction will send it to Sentry
  console.log('trans finish');
};
